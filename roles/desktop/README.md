# role desktop

Installs desktop packages,

### Debian / Ubuntu

* desktop_packages: List of DEB packages to install.

### RedHat / CentOS

* desktop_packages: List of YUM packages to install.

### Amazon

* desktop_packages: List of YUM packages to install.
* desktop_extras: List of Amazon Linux Extras packages to install.

### MacOSX

* desktop_casks: List of Homebrew Casks to install
